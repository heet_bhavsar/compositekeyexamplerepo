package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Employee;
import com.example.demo.entity.EmployeePk;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, EmployeePk> {

}
