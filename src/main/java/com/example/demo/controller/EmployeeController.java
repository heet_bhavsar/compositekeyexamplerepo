package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@PostMapping("/employees")
	public Employee saveEmployee(@RequestBody Employee employee)
	{
		Employee saveEmployee = this.employeeService.saveEmployee(employee);
		return saveEmployee;
	}
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployee()
	{
		List<Employee> allEmployee = this.employeeService.getAllEmployee();
		return allEmployee;
	}
}
