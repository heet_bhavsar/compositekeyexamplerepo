package com.example.demo.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@Embeddable
public class EmployeePk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String empId;
	private String deptId;
	public EmployeePk() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmployeePk(String empId, String deptId) {
		super();
		this.empId = empId;
		this.deptId = deptId;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "EmployeePk [empId=" + empId + ", deptId=" + deptId + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(deptId, empId);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeePk other = (EmployeePk) obj;
		return Objects.equals(deptId, other.deptId) && Objects.equals(empId, other.empId);
	}
	
	
	
	
	
	

}
