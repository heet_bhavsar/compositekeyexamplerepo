package com.example.demo.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Employee  {

	

	@EmbeddedId
	private EmployeePk employeePk;
	
	private String empName;
	private String empEmail;
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Employee(EmployeePk employeePk, String empName, String empEmail) {
		super();
		this.employeePk = employeePk;
		this.empName = empName;
		this.empEmail = empEmail;
	}
	public EmployeePk getEmployeePk() {
		return employeePk;
	}
	public void setEmployeePk(EmployeePk employeePk) {
		this.employeePk = employeePk;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	@Override
	public String toString() {
		return "Employee [employeePk=" + employeePk + ", empName=" + empName + ", empEmail=" + empEmail + "]";
	}
	
	
	
	
	
	
	
	
}
