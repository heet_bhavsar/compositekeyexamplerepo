package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.example.demo.dao.EmployeeRepo;
import com.example.demo.entity.Employee;
import com.example.demo.entity.EmployeePk;



@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepo employeeRepo;
	
	public Employee saveEmployee(Employee employee)
	{
		Employee save = this.employeeRepo.save(employee);
		return save;
	}
	
	public List<Employee> getAllEmployee()
	{
		List<Employee> findAll = this.employeeRepo.findAll();
		return findAll;
	}
}
